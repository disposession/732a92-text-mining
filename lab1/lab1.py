import string
import random
import math
import argparse
from scipy import *

parser = argparse.ArgumentParser(description='Text Mining lab1 exercises')

parser.add_argument("-e", '--exercise', nargs='+', type=int, help='Specify the exercise(s) to print as integers separated by space')

args = parser.parse_args()
if args.exercise:
    print args.exercise

if not args.exercise or 1 in args.exercise:
    print "--- EXERCISE 1 ---"
    # 1a)
    print "\t 1 a) print the parrot variable"
    parrot = "It is dead, that is what is wrong with it."
    print "\t\t%s" % parrot
    # 1b)
    print "\t 1 b) print the size of the parrot variable."
    print "\t\t%s" % len(parrot)

    # 1c)
    print "\t 1 c) print the number of letters in the parrot variable."
    total_letters = 0
    for c in parrot:
        if c.isalpha():
            total_letters += 1
    print "\t\t%s" % total_letters

    # 1d)
    print "\t 1 d) Split the parrot string into a list of words."
    # Strip parrot of punctuation
    punctuation_set = set(string.punctuation)
    stripped_parrot = ''.join(ch for ch in parrot if ch not in punctuation_set)
    # Split into words
    ParrotWords = stripped_parrot.split(" ")
    print "\t\t%s" % ParrotWords

    # 1e)
    # We'll split the string again, to regain punctuation (removed on last exercise)
    ParrotWords = parrot.split(" ")
    print "\t 1 e) Join the previous list again"
    sentence = " ".join(ParrotWords)
    print "\t\t" + sentence

if not args.exercise or 2 in args.exercise:
    print "--- EXERCISE 2 ---"
    # 2a)
    print "\t 2 a) for loop from 5 to 10"
    for i in range(5, 11):
        print "\t\tThe next number in the loop is %d" % i

    # 2b)
    print "\t 2 b) While loop that creates random number until > 0.9"
    r = random.random()
    while r < 0.9:
        print "\t\tThe random number (%0.2f) is smaller than 0.9" % r
        r = random.random()
    print "\t\t%0.2f" % r, "got it!"

    # 2c)
    print "\t 2 c) Write that people's names are nice"
    names = ['Ludwig', 'Rosa', 'Mona', 'Amadeus']
    for name in names:
        print "\t\t" + "The name %s is nice" % name

    # 2d)
    print "\t 2 d) Count number of characters in names"
    nLetters = list()
    for index, name in enumerate(names):
        nLetters.append(len(name))
    print "\t\t%s" % nLetters

    # 2e)
    print "\t 2 e) Count number of characters in names with list comprehension"
    nLetters = [len(name) for name in names]
    print "\t\t%s" % nLetters

    # 2f)
    print "\t 2 f) Categorize names as short or long"
    shortLong = ["short" if len(name) <= 4 else "long" for name in names]
    print "\t\t%s" % shortLong

    # 2g)
    print "\t 2 g) Print names and their respective categories"
    for tuple in zip(names, shortLong):
        print "\t\tThe name %s is a %s name" % (tuple[0], tuple[1])

if not args.exercise or 3 in args.exercise:
    print "--- EXERCISE 3 ---"
    # 3a)
    print "\t 3 a) Create dictionary with Amadeus information"
    Amadeus = dict(Name="Amadeus", Gender="M", Algebra=8, History=13)
    print "\t\t%s" % Amadeus
    # 3b)
    print "\t 3 b) Create dictionary with Rosa, Mona and Ludwig's information"
    Rosa = dict(Name="Rosa", Gender="F", Algebra=19, History=22)
    Mona = dict(Name="Mona", Gender="F", Algebra=6, History=27)
    Ludwig = dict(Name="Ludwig", Gender="M", Algebra=9, History=5)
    print "\t\t%s" % Rosa
    print "\t\t%s" % Mona
    print "\t\t%s" % Ludwig

    # 3c)
    print "\t 3 c) Agglomerate all info on single dictionary. Print score from student"
    students = dict(Amadeus=Amadeus, Rosa=Rosa, Mona=Mona, Ludwig=Ludwig)
    print "\t\t The student's dictionary is %s" % students
    print "\t\t The Amadeus' History score is %s" % students['Amadeus']['History']

    # 3d)
    print "\t 3 d) Add new student, Karl"
    students['Karl'] = dict(Name="Karl", Gender="M", Algebra=14, History=10)
    print "\t\t%s" % students['Karl']

    # 3e)
    print "\t 3 e) Use for loop to print names and scores"
    for student_name, student_info in students.iteritems():
        print "\t\tStudent %s scored %d on the Algebra exam and %d on the History exam" % (student_name, student_info['Algebra'], student_info['History'])

if not args.exercise or 4 in args.exercise:
    print "--- EXERCISE 4 ---"
    # 4a)
    print "\t 4 a) Create two lists of integers. Try to multiply them"
    list1 = [1,3,4]
    list2 = [5,6,9]
    print "\t\t%s" % list1
    print "\t\t%s" % list2
    try:
        print list1*list2
    except Exception, err:
        print "\t\tError: %s" % err
        print "\t\tThis fails because the multiplication operation is not defined for the list type"

    # 4b)
    print "\t 4 b) Use numpy's array instead of list. Multiply now."

    array1 = asarray(list1)
    array2 = asarray(list2)
    try:
        print "\t\t%s" % (array1*array2)
        print "\t\tIt works!"
    except Exception, err:
        print "\t\tError: %s" % err

    # 4c)
    print "\t 4 c) Use numpy's array class to create matrices. Multiply them"
    matrix1 = array([array1, array2])
    matrix2 = array([[1, 0, 0], [0, 2, 0], [0, 0, 3]])
    try:
            print "Matrix1 * matrix2"
            print "\t\t%s" % (matrix1 * matrix2)
            print "\t\tIt works!"
    except Exception, err:
            print "\t\tError: %s" % err
            print "\t\tThe array object type does not support matrix multiplication through the binary operator *"

    # 4d)
    print "\t 4 d) Print result of matrix multiplication"
    matrix1 = array([array1, array2])
    matrix2 = array([[1,0,0],[0,2,0],[0,0,3]])
    final_matrix = dot(matrix1, matrix2)
    print "\t\t%s" % final_matrix[0]
    print "\t\t%s" % final_matrix[1]

if not args.exercise or 5 in args.exercise:
    print "--- EXERCISE 5 ---"
    # 5a)
    print "\t 5 a) Write function to calculate circle area"

    def CircleArea(radius):
        return math.pi * radius * radius

    print "\t\tCircle area of a circle with radius 3 is %0.2f" % CircleArea(3)

    # 5b)
    print "\t 5 b) Write same function but with parameter validation"
    def CircleArea(radius):
        if radius < 0:
            print "\t\tError: The radius must be positive!"
            return None
        return math.pi * radius * radius
    print "\t\tRunning function with -1 as radius:"
    CircleArea(-1)
    print "\t\tRunning function with 3 as radius:"
    print "\t\t%0.2f" % CircleArea(3)

    # 5c)
    print "\t 5 c) Write function in its own module"
    # See geometry file attachment
    import geometry

    print "\t\tRunning function within its own module, using sides 2 and 4: %0.2f" % geometry.RectangleArea(2, 4)
