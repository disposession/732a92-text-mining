def RectangleArea(base, height):
	if (base < 0 or height < 0):
		print "Parameters should both be positive!"
		return None
	return base*height
