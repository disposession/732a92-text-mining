import itertools

from nltk import BigramAssocMeasures, BigramCollocationFinder


def find_bigrams_in_all_documents(documents, score_fn=BigramAssocMeasures.chi_sq, n=200):
    all_words = list()
    for document in documents:
        for word in document[0]:
            all_words.append(word)
    bigram_finder = BigramCollocationFinder.from_words(all_words)
    bigrams = bigram_finder.nbest(score_fn, n)
    return dict([(ngram, True) for ngram in itertools.chain(all_words, bigrams)])