import nltk
from nltk.tokenize import RegexpTokenizer
from bs4 import BeautifulSoup

nltk.ne_chunk()
use_html2text = False

with open('liu_news.html', 'r') as webpage:
    data = webpage.read()

    parsed_webpage = BeautifulSoup(data, 'lxml')
    main_div = parsed_webpage.find('div', "content-50 content-left")
    main_text = main_div.find('div', 'text')
    # Title
    title = main_text.find('h1').getText()
    # The actual news substance
    final_text = "%s " % title
    for paragraph in main_text.findAll('p')[0:6]:
        this_paragraph = paragraph.getText()
        # print(this_paragraph)
        final_text += "%s " % this_paragraph

    # Segment raw text
    # print final_text
    res = nltk.sent_tokenize(final_text)
    # Remember to always print strings, not lists
    # Otherwise, the weird \u123 things happen

    tokenizer = RegexpTokenizer(r'\w+')
    # print res
    # Tokenize text
    token_list = list()
    for sentence in res:
        token_list.extend(tokenizer.tokenize(sentence))

    for i in range(len(token_list)):
        token_list[i] = token_list[i].lower()
        # Lemmatize
        # print(token_list[i], end=" ")

    # Choose a subset of text (2-3 sentences?)
    gold_standard = [
        ('four', 'CD'),
        ('research', 'NN'),
        ('teams', 'NNS'),
        ('at', 'RP'),
        ('LiU', 'NNP'),
        ('have', 'VBP'),
        ('been', 'VBP'),  # Ask assistant
        ('awarded', 'VBN'),
        ('a', 'DT'),
        ('total', 'PDT'),   # ask assistant
        ('of', 'IN'),
        ('SEK', '$'),
        ('11', 'CD'),
        ('million', 'CD'),
        ('from', 'RP'),
        ('the', 'DT'),
        ('FORTE', 'NNP'),
        ('research', 'NN'),
        ('council', 'NN'),
        ('these', 'DT'),
        ('include', 'VBP'),
        ('a', 'DT'),
        ('team', 'NN'),
        ('working', 'VBG'),
        ('on', 'IN'),
        ('a', 'DT'),
        ('project', 'NN'),
        ('looking', 'VBG'),
        ('at', 'RP'),
        ('what', 'WDT'),   # ask assistant
        ('mental', 'NN'),
        # ('ill-health', 'JJ'), This should be a single word, instead is tokenized as two. Thoughts?
        ('ill', 'JJ'),
        ('health', 'NN'),
        # ^
        ('means', 'VBZ'),
        ('to', 'TO'),
        ('young', 'JJ'),
        ('people', 'NNS'),
        ('and', 'CC'),
        ('another', 'DT'),
        ('analysing', 'VBG'),
        ('how', 'WRB'),
        ('easy', 'JJ'),
        ('people', 'NNS'),
        ('can', 'MD'),
        ('obtain', 'VB'),
        ('assisted', 'JJ'),
        ('conception', 'NN'),
        ('Anette', 'NNP'),
        ('Wickström', 'NNP'),
        ('associate', 'JJ'),
        ('professor', 'NN')
    ]
    # Use manual tagging as gold standard to compare two different taggers
    confusion_matrix = dict()
    tagged = nltk.pos_tag(token_list)
    ne_tagged = nltk.ne_chunk(token_list, binary=True)
    # print(tagged[9:56])
    # print(gold_standard)
    size = len(gold_standard)
    correct_token = 0
    for i in range(len(gold_standard)):
        # print("%s - %s " % (gold_standard[i][0], tagged[i+9][0]))
        if gold_standard[i][1] == tagged[i+9][1]:
            correct_token += 1

    print(correct_token/size)
    # >>> continued in Jupyter notebook